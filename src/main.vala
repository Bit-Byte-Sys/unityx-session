/* main.vala
 *
 * Licensed under GNU General Public License v3.0.
 */

using GLib;
using Posix;

int main (string[] args) {
    // Check arguments length.
	if (args.length < 2) {
		GLib.stderr.printf ("%s\n", "Requires 1 argument: session path");
		return 1;
	}

    var session_path = args[1];

    // Create configuration KeyFile object.
    KeyFile config = new KeyFile ();

    // Load configuration file into 'config' object.
    try
    {
        config.load_from_file (session_path, KeyFileFlags.NONE);
        Environment.set_variable ("GTK_THEME", config.get_string("SessionData", "GTK_Theme"), true);
        system(config.get_string("SessionData", "Exec"));
    }
	catch (FileError e)
	{
	    // Output error.
		GLib.stderr.printf ("%s%s\n", "Failed to read session file: ", session_path);
	}
    catch (KeyFileError e)
    {
	    // Output error.
		GLib.stderr.printf ("%s%s\n", "Failed to parse session file: ", session_path);
    }
    return 0;
}
